 var express = require('express');
 var router = express.Router();
 var models = require('../models/schema');
 var mongoose = require("mongoose");
 router.route('/')
  .get(function(req, res){
    res.render('index')
  })
  .post(function(req,res){
    models.User({
  		name : req.body.name,
  		email : req.body.email,
  		password : req.body.password
  	}).save(function(error){
  	if(error) res.send("error")
  		else res.redirect("/");
  	})
 })
 module.exports = router;
